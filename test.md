---
title: "Test"
author: "Waldir"
date: "23 8 2019"
output: 
  html_document: 
    keep_md: yes
---

# Title

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.


```r
summary(cars)
```

```
##      speed           dist       
##  Min.   : 4.0   Min.   :  2.00  
##  1st Qu.:12.0   1st Qu.: 26.00  
##  Median :15.0   Median : 36.00  
##  Mean   :15.4   Mean   : 42.98  
##  3rd Qu.:19.0   3rd Qu.: 56.00  
##  Max.   :25.0   Max.   :120.00
```

# Including Plots

You can also embed plots, for example:

![](test_files/figure-html/pressure-1.png)<!-- -->

Do equations work?

$$
x = 1
$$

Multiline?

$$
x = 2 \\
y = z
$$

\LaTeX tags? Equations?

\begin{equation}
    \sum_{i = 1}^n p_i
\end{equation}
